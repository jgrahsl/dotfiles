vim.o.termguicolors = true
vim.o.syntax = 'on'
vim.o.errorbells = false
vim.o.smartcase = true
vim.o.showmode = false
vim.bo.swapfile = false
vim.o.backup = false
vim.o.undodir = vim.fn.stdpath('config') .. '/undodir'
vim.o.undofile = true
vim.o.incsearch = true
vim.o.hidden = true
vim.o.completeopt = 'menuone,noinsert,noselect'
vim.bo.autoindent = true
vim.bo.smartindent = true
vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true
vim.wo.number = true
-- vim.wo.relativenumber = true
vim.g.mapleader = ' '
vim.wo.signcolumn = 'yes'
vim.wo.wrap = false
--#vim.opt.iskeyword:remove { "_" }
vim.o.number = true
vim.o.textwidth = 0
vim.o.wrapmargin = 0
vim.o.wrap = true
vim.o.linebreak = true

--function _G.put(...)
--  local objects = {}
--  for i = 1, select('#', ...) do
--    local v = select(i, ...)
--    table.insert(objects, vim.inspect(v))
--  end
--
--  print(table.concat(objects, '\n'))
--  return ...
--end

local key_mapper = function(mode, key, result)
  vim.api.nvim_set_keymap(
    mode,
    key,
    result,
    { noremap = true, silent = true }
  )
end

--
-- Navigation
--
key_mapper('', '<up>', '<nop>')
key_mapper('', '<down>', '<nop>')
key_mapper('', '<left>', '<nop>')
key_mapper('', '<right>', '<nop>')
key_mapper('i', 'jk', '<ESC>')
key_mapper('i', 'JK', '<ESC>')
key_mapper('i', 'jK', '<ESC>')

key_mapper("n", "<S-h>", ":bprevious<CR>")
key_mapper("n", "<S-l>", ":bnext<CR>")

--
-- Telescope
--
--key_mapper('n', '<leader>fl', '<cmd>Telescope lsp_document_symbols<CR>')
key_mapper('n', '<leader>ff', '<cmd>Telescope find_files<CR>')
key_mapper('n', '<leader>fg', '<cmd>Telescope live_grep<cr>')
key_mapper('n', '<leader>fw', '<cmd>Telescope grep_string<cr>')
key_mapper('n', '<leader>fb', '<cmd>Telescope buffers<cr>')
--key_mapper('n', '<leader>fh', '<cmd>Telescope help_tags<cr>')
key_mapper('n', '<leader>fj', '<cmd>Telescope current_buffer_fuzzy_find<cr>')
key_mapper('n', '<leader>fk', '<cmd>Telescope keymaps<cr>')
key_mapper('n', '<leader>fo', '<cmd>Telescope oldfiles<cr>')
---key_mapper('n', '<leader>fq', '<cmd>Telescope quickfix<cr>')

-- key_mapper('n', '<leader>fp', '<cmd>lua require("telescope").extensions.project.project{}<CR>')
key_mapper('n', '<leader>fn', '<cmd>Telescope file_browser path=%:p:h select_buffer=true<CR>')
key_mapper('n', '<leader>fm', '<cmd>Telescope marks<cr>')

-- Harpoon
key_mapper('n', '<leader>ha', ':lua require("harpoon.mark").add_file()<CR>')
key_mapper('n', '<leader>hm', ':lua require("harpoon.ui").toggle_quick_menu()<CR>')
key_mapper('n', '<leader>h1', ':lua require("harpoon.ui").nav_file(1)<CR>')
key_mapper('n', '<leader>h2', ':lua require("harpoon.ui").nav_file(2)<CR>')
key_mapper('n', '<leader>h3', ':lua require("harpoon.ui").nav_file(3)<CR>')
key_mapper('n', '<leader>h4', ':lua require("harpoon.ui").nav_file(4)<CR>')

-- Debug
key_mapper('n', '<leader>du', ':lua require("dapui").toggle()<CR>')
key_mapper('n', "<leader>dd", ":lua require('telescope').extensions.dap.commands{}<CR>")
key_mapper("n", "<leader>db", ":lua require('telescope').extensions.dap.list_breakpoints{}<CR>")

key_mapper("n", "gO", ":lua vim.lsp.buf.document_symbol()<CR>")

vim.keymap.set('n', '<leader>dj',
  function()
    path = vim.fn.input('Path to json: ', vim.fn.getcwd() .. '/', 'file')
    require("dap.ext.vscode").load_launchjs(path)
  end
)

vim.api.nvim_set_keymap('n', '<C-n>', ':NvimTreeFocus<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<C-j>', ':NvimTreeToggle<CR>', { noremap = true, silent = true })

vim.api.nvim_set_keymap('n', '<C-m>', ':SymbolsOutlineOpen<CR>', { noremap = true, silent = true })

--
-- Plugins Setup
--
require("plugins").setup()

local so_opts = {
  highlight_hovered_item = true,
  show_guides = true,
  auto_preview = false,
  position = 'right',
  relative_width = true,
  width = 25,
  auto_close = false,
  show_numbers = false,
  show_relative_numbers = false,
  show_symbol_details = true,
  preview_bg_highlight = 'Pmenu',
  autofold_depth = 0,
  auto_unfold_hover = true,
  fold_markers = { '', '' },
  wrap = false,
  keymaps = {
              -- These keymaps can be a string or a table for multiple keys
    close = { "<Esc>", "q" },
    goto_location = "<Cr>",
    focus_location = "o",
    hover_symbol = "<C-space>",
    toggle_preview = "K",
    rename_symbol = "r",
    code_actions = "a",
    fold = "h",
    unfold = "l",
    fold_all = "W",
    unfold_all = "E",
    fold_reset = "R",
  },
  lsp_blacklist = {},
  symbol_blacklist = {},
  symbols = {
    File = { icon = "", hl = "@text.uri" },
    Module = { icon = "", hl = "@namespace" },
    Namespace = { icon = "", hl = "@namespace" },
    Package = { icon = "", hl = "@namespace" },
    Class = { icon = "𝓒", hl = "@type" },
    Method = { icon = "ƒ", hl = "@method" },
    Property = { icon = "", hl = "@method" },
    Field = { icon = "", hl = "@field" },
    Constructor = { icon = "", hl = "@constructor" },
    Enum = { icon = "ℰ", hl = "@type" },
    Interface = { icon = "", hl = "@type" },
    Function = { icon = "", hl = "@function" },
    Variable = { icon = "", hl = "@constant" },
    Constant = { icon = "", hl = "@constant" },
    String = { icon = "𝓐", hl = "@string" },
    Number = { icon = "#", hl = "@number" },
    Boolean = { icon = "⊨", hl = "@boolean" },
    Array = { icon = "", hl = "@constant" },
    Object = { icon = "⦿", hl = "@type" },
    Key = { icon = "🔐", hl = "@type" },
    Null = { icon = "NULL", hl = "@type" },
    EnumMember = { icon = "", hl = "@field" },
    Struct = { icon = "𝓢", hl = "@type" },
    Event = { icon = "🗲", hl = "@type" },
    Operator = { icon = "+", hl = "@operator" },
    TypeParameter = { icon = "𝙏", hl = "@parameter" },
    Component = { icon = "", hl = "@function" },
    Fragment = { icon = "", hl = "@constant" },
  },
}

require("symbols-outline").setup(so_opts)

-- lspconfig
--

local lspconfig = require('lspconfig')

vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function(event)
    local opts = { buffer = event.buf }

    vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
    vim.keymap.set('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
    vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
    vim.keymap.set("n", "gr", "<cmd>lua require('nice-reference').references()<CR>", opts)
    vim.keymap.set('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<cr>', opts)
    vim.keymap.set('n', 'go', '<cmd>lua vim.lsp.buf.type_definition()<cr>', opts)
    vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
    vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
    vim.keymap.set({ 'n', 'x' }, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
    vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
  end
})

local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local default_setup = function(server)
  require('lspconfig')[server].setup({
    capabilities = lsp_capabilities,
  })
end

  require("nvim-tree").setup({
    sort_by = "case_sensitive",
    view = {
--      width = 30,
      adaptive_size = true
    },
    renderer = {
      group_empty = true,
    },
    filters = {
      dotfiles = false,
    },
  })

require('mason').setup({})
require('mason-lspconfig').setup({
  ensure_installed = {},
  handlers = {
    default_setup,
  },
})

--local on_attach = function(client, bufnr)
--   local bufopts = { noremap=true, silent=true, buffer=bufnr }
--end

lspconfig.ccls.setup {
  --  on_attach = on_attach;
  init_options = {
    compilationDatabaseDirectory = "build",
    index = {
      threads = 0,
    },
    clang = {
      excludeArgs = { "-frounding-math" },
    },
  }
}

lspconfig.rust_analyzer.setup {
  settings = {
    cmd = "~/.rustup/toolchains/nightly-x86_64-apple-darwin/bin/rust-analyzer",
    ['rust-analyzer'] = {
      diagnostics = {
        enable = false,
      }
    }
  }
}

-- Surpress warning about vim globals
require 'lspconfig'.lua_ls.setup {
  -- ... other configs
  settings = {
    Lua = {
      diagnostics = {
        globals = { 'vim' }
      }
    }
  }
}
--
-- Completion
--

local cmp = require("cmp")
local luasnip = require("luasnip")
-- Basic mapping
--
--  mapping = cmp.mapping.preset.insert({
--   ['<C-g>'] = cmp.mapping.scroll_docs(-4),
--   ['<C-f>'] = cmp.mapping.scroll_docs(4),
--   ['<C-o>'] = cmp.mapping.complete(),
--   ['<C-e>'] = cmp.mapping.abort(),
--   ['<CR>']  = cmp.mapping.confirm({ cmp.ConfirmBehavior.Replace, select = true }),
--   ['<TAB>'] = cmp.mapping.select_next_item(),
-- })
--
cmp.setup({
  snippet = {
    expand = function(args) 
      luasnip.lsp_expand(args.body)
    end
  },
  sources = cmp.config.sources({
    { name = "nvim_lsp" }, -- LSP
    { name = "luasnip" }, -- snippets
    { name = "buffer" }, -- text within the current buffer
    { name = "path" }, -- file system paths
  }),
  mapping = cmp.mapping.preset.insert({
    ["<C-p>"] = cmp.mapping.select_prev_item(),
    ["<C-n>"] = cmp.mapping.select_next_item(),
    ["<C-u>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
    ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
    ["<C-c>"] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    ["<CR>"] = cmp.mapping.confirm({ select = true }),

    ["<Right>"] = cmp.mapping.confirm({ select = true }),
    ["<Tab>"] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_next_item()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_prev_item()
      end
    end, {
      "i",
      "s" })
  })
})

--
-- Folding
--
--vim.api.nvim_create_autocmd({ 'BufEnter', 'BufAdd', 'BufNew', 'BufNewFile', 'BufWinEnter' }, {
--  pattern = { "*.md" },
--  callback = function()
--    print("Hallo")
--    vim.opt.foldmethod = 'expr'
--    vim.opt.foldexpr   = 'nvim_treesitter#foldexpr()'
--  end
--})
----
--vim.api.nvim_create_autocmd({ 'BufReadPost,FileReadPost' }, {
--  pattern = { "*.md" },
--  callback = function()
--    vim.api.nvim_win_set_option(0, "foldlevel", 20)
--  end
--})
--
function _G.toggle_fold()
  local get_opt = vim.api.nvim_win_get_option
  local set_opt = vim.api.nvim_win_set_option

  if get_opt(0, "foldlevel") >= 20 then
    set_opt(0, "foldlevel", 0)
  else
    set_opt(0, "foldlevel", 20)
  end
end

key_mapper('n', "<Leader><Tab>", ':lua toggle_fold()<CR>')

----
---- Norg
----
--require('orgmode').setup_ts_grammar()
----
--require('nvim-treesitter.configs').setup {
--  --  -- If TS highlights are not enabled at all, or disabled via `disable` prop,
--  --  -- highlighting will fallback to default Vim syntax highlighting
--  highlight = {
--    --    enable = true,
--    --    -- Required for spellcheck, some LaTex highlights and
--    --    -- code block highlights that do not have ts grammar
--    additional_vim_regex_highlighting = { 'org' },
--  },
--  ensure_installed = { 'org' }, -- Or run :TSUpdate org
--}
----
--require('orgmode').setup({
--  org_agenda_files = { '~/org/*' },
--  org_default_notes_file = '~/org/refile.org',
--})

-- LUA Pad
--
require('luapad').setup({})

-- Goto Preview
--
require('goto-preview').setup({default_mappings = true})

-- Nice reference
-- 
require 'nice-reference'.setup({
    anchor = "NW", -- Popup position anchor
    relative = "cursor", -- Popup relative position
    row = 1, -- Popup x position
    col = 0, -- Popup y position
    border = "rounded", -- Popup borderstyle
    winblend = 0, -- Popup transaparency 0-100, where 100 is transparent
    max_width = 120, -- Max width of the popup
    max_height = 10, -- Max height of the popup
    auto_choose = false, -- Go to reference if there is only one
})


