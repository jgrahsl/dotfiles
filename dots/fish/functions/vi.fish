function vi --wraps=vim --wraps=/usr/local/bin/nvim --description 'alias vi /usr/local/bin/nvim'
  /usr/local/bin/nvim $argv
        
end
