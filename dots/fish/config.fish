if status is-interactive
    # Commands to run in interactive sessions can go here

    set fish_greeting
    set -gx LSCOLORS gxfxcxdxbxegedabagacad
    set -g fish_term24bit 1

    set host_config ~/.config/fish/config.(hostname).fish
    test -r $host_config; and source $host_config
    set -e host_config
end
