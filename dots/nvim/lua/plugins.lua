local M = {}

function M.setup()
  -- Indicate first time installation
  local packer_bootstrap = false

  -- packer.nvim configuration
  local conf = {
    display = {
      open_fn = function()
        return require("packer.util").float { border = "rounded" }
      end,
    },
  }

  -- Check if packer.nvim is installed
  -- Run PackerCompile if there are changes in this file
  local function packer_init()
    local fn = vim.fn
    local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
    if fn.empty(fn.glob(install_path)) > 0 then
      packer_bootstrap = fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
      }
      vim.cmd [[packadd packer.nvim]]
    end
    vim.cmd "autocmd BufWritePost plugins.lua source <afile> | PackerCompile"
  end

-- Plugins
  local function plugins(use)
    use { "wbthomason/packer.nvim" }

    use "nvim-lua/plenary.nvim"
    use "nvim-lua/popup.nvim"
-- Treesitter
    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }
-- Leap
    use { "ggandor/leap.nvim",
          config = function()
             require('leap').add_default_mappings()
          end
    }
-- Harpoon
   use {
        "ThePrimeagen/harpoon",
        keys = { [[<leader>h]] },
        module = { "harpoon", "harpoon.cmd-ui", "harpoon.mark", "harpoon.ui", "harpoon.term" },
        wants = { "telescope.nvim" },
        config = function()
            require("config.packages.harpoon").setup()
        end,
    }

    use { 'rafcamlet/nvim-luapad' }

-- Colorscheme
    use {
      "sainnhe/everforest",
      config = function()
        vim.cmd "colorscheme everforest"
      end,
    }
    
-- telescope dap
    use { "nvim-telescope/telescope-dap.nvim" } --

-- telescope
    use {
            "nvim-telescope/telescope.nvim",
            opt = false,
            disable = false, 
            config = function()
                require("config.packages.telescope").setup()
            end,
            cmd = { "Telescope" },
            module = "telescope",
            keys = { "<leader>f" },
            wants = {
                "plenary.nvim",
                "popup.nvim",
            },
            requires = {
                "nvim-lua/popup.nvim",
                "nvim-lua/plenary.nvim",
                "nvim-telescope/telescope-project.nvim",
                "nvim-telescope/telescope-file-browser.nvim",
            },
    }

    use { 'neovim/nvim-lspconfig' }
    use { 'williamboman/mason.nvim' }
    use { 'williamboman/mason-lspconfig.nvim' }

    use { 'hrsh7th/nvim-cmp' }
    use { 'hrsh7th/cmp-buffer' }
    use { 'hrsh7th/cmp-path' }
    use { 'saadparwaiz1/cmp_luasnip' }
    use { 'hrsh7th/cmp-nvim-lsp' }
    use { 'hrsh7th/cmp-nvim-lua' }
    use { 'L3MON4D3/LuaSnip' }
    use {"rafamadriz/friendly-snippets"}

    use { 'rmagatti/goto-preview'}
-- LSP lsp-zero
--    use {
--            'VonHeikemen/lsp-zero.nvim',
--            config = function()
--                require("config.packages.lsp-zero").setup()
--            end,
--            requires = {
--                -- LSP Support
--                { 'neovim/nvim-lspconfig' },
--                { 'williamboman/mason.nvim' },
--                { 'williamboman/mason-lspconfig.nvim' },
--
--                -- Autocompletion
--                { 'hrsh7th/nvim-cmp' },
--                { 'hrsh7th/cmp-buffer' },
--                { 'hrsh7th/cmp-path' },
--                { 'saadparwaiz1/cmp_luasnip' },
--                { 'hrsh7th/cmp-nvim-lsp' },
--                { 'hrsh7th/cmp-nvim-lua' },
--
--                -- Snippets
--                { 'L3MON4D3/LuaSnip' },
--                { 'rafamadriz/friendly-snippets' },
--            }
--    }

-- Debugging
    use {
        'mfussenegger/nvim-dap',
        config = function()
           require("config.packages.dap").setup()
        end,
        }

    use {
        'rcarriga/nvim-dap-ui',
        config = function()
           require("config.packages.dap-ui").setup()
        end,
        }
-- NerdTree
    use {
      'nvim-tree/nvim-tree.lua',
      requires = {
        'nvim-tree/nvim-web-devicons', -- optional
      }   
    }

-- nice-references
    use {
        'wiliamks/nice-reference.nvim',
    }

    use 'rqdmap/symbols-outline.nvim'

-- DAPUI Dependency ?
    use { "nvim-neotest/nvim-nio" }

-- Norg
--    use {'nvim-orgmode/orgmode', config = function()
--      require('orgmode').setup{}
--    end
--    }
--
    if packer_bootstrap then
      print "Restart Neovim required after installation!"
      require("packer").sync()
    end
  end

  packer_init()

  local packer = require "packer"
  packer.init(conf)
  packer.startup(plugins)
end

return M
