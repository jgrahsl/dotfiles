# Memory Layout

All values in Rust are stack allocated by default.

When packing a value into a box, it is allocated on the heap.

## References

- Reference memory without taking ownership
- References in structs can be a real hassle. Better use owned values.

- Pointer
- &s

## Slices

Reference memory without taking ownership

- Points into contiguous memory
- &s[a..b]
- Pointer
- Length

## Box<T>

Allocates a value T on the heap and takes care of freeing when the Box value goes out of scope. The box stores the pointer to the heap on the stack.

- Pointer into heap memory
- Allocates from the Global Allocator
- Frees when destructor is called