#!/bin/bash

# Relative path to the dots folder within git repo
DOTS=dots

# Abs path to DOTS
BASE=$(realpath $DOTS)

function do_link()
{
    local SRC=$1
    local DST=$2

    if [ ! -s $DST -a ! -f $DST -a ! -d $DST ]; then
        echo "Linking $SRC to $DST"
	    ln -fs $SRC $DST
    else
        echo "Skipping linking $SRC to $DST"
    fi
} 

### Bulk dot file going directly under HOME

# Relative path of the to-be-linked dot files within DOTS
FILES=" \
	.gitconfig \
	.tigrc \
	.tmux.conf \
	.tmux \
	.tmuxp"

for FILE in $FILES;
do
    SRC=$BASE/$FILE 
    DST=$HOME/$FILE

    do_link $SRC $DST
done

### Nested dot file custom installation steps

#### SSH

FILE=git-hosts.inc
SUBDIR=.ssh

mkdir -p ~/$SUBDIR
if [ ! -f ~/$SUBDIR/config ]; then
    echo "" > ~/$SUBDIR/config
fi

SRC=$BASE/$FILE 
DST=~/$SUBDIR/$FILE

do_link $SRC $DST

SSH_CONFIG_LINE="Include $FILE"
grep "$SSH_CONFIG_LINE" ~/$SUBDIR/config > /dev/null || sed -i "1 i$SSH_CONFIG_LINE" ~/$SUBDIR/config

#### FISH

FILE=fish
SUBDIR=.config

mkdir -p ~/$SUBDIR

SRC=$BASE/$FILE 
DST=~/$SUBDIR/$FILE

do_link $SRC $DST

do_link ~/.fzf/shell/key-bindings.fish ~/.config/fish/functions/fzf_key_bindings.fish

#### NVIM

FILE=nvim
SUBDIR=.config

mkdir -p ~/$SUBDIR

SRC=$BASE/$FILE 
DST=~/$SUBDIR/$FILE

do_link $SRC $DST

fish -c "fisher update"
nvim +PackerInstall +PackerUpdate +q
nvim +PackerCompile +q
nvim +TSUpdate +q


