# Learning Rust

## General

- Statically typed
- No runtime
- Functional pradadigm
- Rust is an "eager" language

## Memory

- static data means known size and is allocated on stack
- all values are on the stack with two exceptions:
 - data is on the heap if the size is unknown during compile time
 - data is on the head if a Box is created
- a pointer is saved on the stack for heap values

## References and Borrow

- A reference is just a pointer that is assumed to be aligned, not null, and pointing to memory containing a valid value of T

- A reference represents a borrow of some owned value. You can get one by using the & or &mut operators on a value.

- Along side a control flow, the same data is referenced by different names. To avoid copying, a reference is passed.

- References have a lifetime attached to them, which represents the scope for which the borrow is valid.

### Copy and Clone - Move, Copy, Clone

The assignment operator in Rust either moves values or does trivial bitwise copies. Never a deep copy. Same for function arguments and returns.

- derive(Clone, Copy)

### Copy

- Types which do not own other resources (on the heap) and can be bitwise copied are called `Copy` types. They implement the Copy marker trait. All primitive types like integers, floats and characters are Copy. Structs or enums are not Copy by default but you can derive the Copy trait (all the members of the struct or enum must be Copy).

### Moves

- When the variable v is moved to v1, the object on the stack is `bitwise copied`. The underlying memory on the heap `is _transfered_ to v1`. Sepcifically to drop the resource at the appropriate time.

### Clones

To make a `deep copy` (of structs that do own memory on the heap), client code should call the clone method. clones are free to independently drop their heap buffers.

### Drop

In general, any type that implements Drop cannot be Copy because Drop is implemented by types which own some resource and hence cannot be simply bitwise copied. But Copy types should be trivially copyable. Hence, Drop and Copy don't mix well.

## Types

- `::<>` turbofish operator is used after a `fn` call to make explicit the returned type if it cannot be inferred. Use this or declare the type in the left hand side of the assignement.

### Casting

- `as` can only be used for primitive types or to coerce to a specific trait

### Box

- A box is just a reference to some memory in the heap.
- the `dyn` keyword is used to signal that the type is a trait and only concrete implementations define its size

## Error handling

- Do not use `unwrap`
- Match the error explicitly
- Use `unwrap_or` or `unwrap_or_else`. The later is only evaluated if needed.
- Libraries should use Result together with something implementing std::error::Error

### unwrap vs expect

- unwrap panics with the message in the Err (for a result)
- expect panics with a custome error message

### Error propagation

- `?` is a shorthand for a match statement that either unwraps the `Ok` value or returns from the `fn` with the `Err` value

## Strings

- Rust has 6 types of strings: String, Str, CString, Cstr, OsString, Ostr
- String is to &str like Vec<T> to &[T] (array)

- "string literals" in code are `&'static str` are known to live longer than `main`

- Use `&str` return type if it is a substring of some other string, use `String` otherwise
- `&str`: Unlike its String counterpart, its contents are borrowed.

## Slices

- Their length is fixed at runtime
- Views into arrays, vectors and str
- Memory layout is a pointer plus a length
- Point into contiguous memory with fixed distance between elements

## Array

- Type signature [T; length]
- Array is size is fixed at compile time
- An array is a collection of objects of the same type T
- Stored in contiguous memory with fixed distance between elements

## Deref trait

Deref coercion

## Modules

Using a semicolon after mod front_of_house rather than using a block tells Rust to load the contents of the module from another file with the same name as the module.

There are two distinct things: fs hierachy and module tree as seen by the compiler.

There is no implicit mapping between fs hierachy and module tree.

The `mod` keyword declares a file as a submodule inside a _different_ file.

We can use the `mod my_module` keyword only for my_module.rs or my_module/mod.rs in the same directory.

When using subdirectories, a `mod.rs` inside the subdirectory is necessary

When accessing modules which are not located on the same level or direct child levels of a module, the absolute path with respect to the crate route needs to be used `crate::subdir1::subdir2::modulename

One can also use the `super` keyword for relative paths.
