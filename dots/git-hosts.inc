Host smartmeter
     Hostname 192.168.0.234

     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     User julian
     AddKeysToAgent yes

Host nwbus
     Hostname 192.168.0.80
     ProxyCommand ssh l3 -l julian -W %h:%p

     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     User julian
     AddKeysToAgent yes
     
Host rain
     Hostname 192.168.0.192
     ProxyCommand ssh l3 -l julian -W %h:%p

     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     User julian
     AddKeysToAgent yes

Host bft
     Hostname 127.0.0.1
     Port 8888
     ProxyCommand ssh l3 -l julian -W %h:%p

     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     User julian
     AddKeysToAgent yes


Host bf
     Hostname bf.grahsl.net
     Port 64000
     LocalForward 18080 127.0.0.1:8080

     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     User julian
     AddKeysToAgent yes


Host l3
     Hostname l3.grahsl.net
     Port 64000

     LocalForward 8080 127.0.0.1:8080
     # zigbee2mqtt l3
     LocalForward 8081 127.0.0.1:8081
     # html server
     LocalForward 1880 127.0.0.1:1880
     # nodered
     LocalForward 18080 127.0.0.1:18080
     # zigbee2mqtt bf
     LocalForward 3000 127.0.0.1:3000
     # grafana 
     LocalForward 8086 127.0.0.1:8086
     # influxdb webui 
     LocalForward 1884 127.0.0.1:1884
     # mqtt ws bridge
     LocalForward 1883 127.0.0.1:1883
     # mqtt
     LocalForward 8443 127.0.0.1:8443
     # mqtt

     User julian
     ForwardAgent yes
     ServerAliveInterval 100
     ServerAliveCountMax 2
     
